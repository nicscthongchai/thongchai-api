using Microsoft.EntityFrameworkCore;

namespace TodoAPI.Models
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options) : base(options) { }
        public DbSet<Todo> Todos { get; set; }
    }

    public class Todo
    {
        public int TodoId { get; set; }
        public string Text { get; set; }
        public bool Completed { get; set; }
    }
}
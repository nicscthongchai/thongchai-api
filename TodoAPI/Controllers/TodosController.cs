using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TodoAPI.Models;

namespace TodoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodosController : ControllerBase
    {
        private readonly TodoContext _context;

        public TodosController(TodoContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            return Ok(await _context.Todos.ToListAsync());
        }

        [HttpPost]
        public async Task<ActionResult> InsertTodoAsync([FromBody]Todo todo)
        {
            _context.Add(todo);
            await _context.SaveChangesAsync();
            return Created("localhost:5000/api/todos/" + 1, new { message = "success" });
        }
    }
}
